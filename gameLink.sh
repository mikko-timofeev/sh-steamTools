#!/bin/sh

games_dir="/home/$USER/Games"
info_dir="./gameInfo"

chk_file(){
    if [ -f "$1" ]; then
        return 0
    fi
    return 1
}

chk_dir(){
    if [ -d "$1" ]; then
        return 0
    fi
    return 1
}

if [ -z ${1+x} ]; then
    read -p "Game ID: " steam_id
else
    steam_id=$1
fi

steam_apps="/home/$USER/.steam/steam/steamapps"
steam_data="$steam_apps/common"

info_file="$info_dir/$steam_id.txt"
if ! ( chk_file "$info_file" ); then
    echo "Game info file '$steam_id' was not found."
    exit 1
fi

game_title="$(awk 'NR==1' $info_file)"
if ! ( chk_dir "$steam_apps" ); then
    echo "Game '$game_title' was not found."
    exit 1
fi

game_files="$steam_data/$(awk 'NR==2' $info_file)"
game_setts="$steam_apps/$(awk 'NR==3' $info_file)"

game_dir="$games_dir/$game_title"
if chk_dir "$game_dir"; then
    echo "Game '$game_title' is already linked. Aborting"
    exit 1
fi
mkdir -p "$game_dir"
cd "$game_dir"

ln -s "$game_files" "$game_dir"
data_dir_name=$(basename "$game_files")
mv "$data_dir_name" "Files"

ln -s "$game_setts" "$game_dir"
settings_dir_name=$(basename "$game_setts")
mv "$settings_dir_name" "Settings"

echo "Game '$game_title' was successfully linked"

# gameLink.sh
```
Usage:  gameLink.sh [argument]  - link Steam game by ID
        gameLink.sh             - interactively link Steam game by ID
```
Steam game definitions are to be placed into `gameInfo` folder as txt files.
Filename: `<steam_id>.txt`
Content of definition file (line by line):

```
<preferred_game_title>
<game_files_directory>
<game_settings_directory>

```

## Examples:
Link Fallout 4 (first time):
```
$ ./gameLink.sh
Game ID: 377160
Game 'Fallout 4 GOTY' was successfully linked
$

```

Link Fallout 4 (already linked):
```
$ ./gameLink.sh 377160
Game 'Fallout 4 GOTY' is already linked. Aborting
$
```

Link unknown game (no game-info file):
```
$ ./gameLink.sh 377161
Game info file '377161' was not found.
$
```
...
